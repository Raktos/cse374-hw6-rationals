/*
 * Implementation of rational number type
 * CSE 374 hw6, 16wi
 * Author: Jason Ho
 * 10 March 2016
 */

#include "rational.h"
#include <stdlib.h>

int greatestCommonDivisor(int a, int b);
int leastCommonMultiple(int a, int b);

Rational::Rational() : Rational(0, 1) {}

Rational::Rational(int n) : Rational(n, 1) {}

Rational::Rational(int n, int d) {
  int divisor = greatestCommonDivisor(n, d);

  this->num = n / divisor;
  this->denom = d / divisor;
}

int Rational::n() {
  return this->num;
}

int Rational::d() {
  return this->denom;
}

Rational Rational::plus(Rational other) {
  int lcm = leastCommonMultiple(this->d(), other.d());
  int num = (this->n() * (lcm / this->d())) + (other.n() * (lcm / other.d()));
  int denom = this->d() * (lcm / this->d());
  return Rational(num, denom);
}

Rational Rational::minus(Rational other) {
  return this->plus(Rational(other.n() * -1, other.d()));
}

Rational Rational::times(Rational other) {
  return Rational(this->n() * other.n(), this->d() * other.d());
}

Rational Rational::div(Rational other) {
  return this->times(Rational(other.d(), other.n()));
}

int greatestCommonDivisor(int a, int b) {
  return b == 0 ? a : greatestCommonDivisor(b, a % b);
}

int leastCommonMultiple(int a, int b) {
  return abs(a * b) / greatestCommonDivisor(a, b);
}
